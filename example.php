<?PHP

include '../logit/logIt.class.php';

// Set process name for logging and possibly other classes (optional)
define('PROCESS', 'example');

// Default logfile in config file somewhere instead of providing it manually in all PHP files
$config['logfile'] = '/tmp/logfile';

// Define the class object
$logIt = new logIt( '/tmp/logfile' );

// Optional configuration parameters
$logIt->processName('example'); // Defaults to file name or PROCESS as defined above

// Always logs to apache error_log. When called from commandline this outputs to screen instead.
$logIt->logToErrorLog();

// For debugging - outputs to screen (browser)
$logIt->logToScreen();

// If none of the above 2 functions are called, log will be written to the provided logfile


// Do some logging
$logIt->log('This is a test', 'ERROR');
