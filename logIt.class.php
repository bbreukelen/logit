<?
class logIt {
	protected $logfile;
	public $process;
	protected $levels;
	protected $logToErrorLog = false;
	protected $logToScreen = false;

	public function processName($name) { $this->process = $name; }
	public function logLevels($levels) { $this->levels = $levels; }
	public function logToErrorLog()		 { $this->logToErrorLog = true; }
	public function logToScreen()      { $this->logToScreen = true; }

  ///////////////////////////////////////////////////////////////////////////
	public function __construct($logfile='') {
		global $config;
		if (isset($logfile) && $logfile != '') {
			$this->logfile = $logfile;
		} else {
			$this->logfile = (isset($config['logfile']) ? $config['logfile'] : '');
		}

		if (defined('PROCESS')) {
			$this->process = PROCESS;
		} else {
			$this->process = basename($_SERVER['PHP_SELF']);
		}
	}

	public function log($msg, $level) {
		if ($this->levels && ! in_array($level, $this->levels) ) { return; }
		$string = ($this->logToErrorLog ? '' : date('Y-m-d H:i:s (T)') . " - ") . "{$this->process} - {$level} - {$msg}" . ($this->logToErrorLog ? '' : "\n");

		if ($this->logToScreen) {
			print $string;
			return;
		}

		if ($this->logToErrorLog || $this->logfile == '') {
			error_log($string);
			return;
		}

		$lf = @fopen($this->logfile, 'a');
		if (! $lf) {
			error_log("Logfile could not be opened: {$this->logfile}");
			return;
		}

		fwrite($lf, $string);
		fclose($lf);
	}
}
?>
